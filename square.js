import * as THREE from "three";
import * as Stats from "stats.js";
import * as dat from "dat.gui";
export function THREECubeAnimation() {
  var renderer;
  var scene;
  var camera;
  var clock;
  var control;

  function init() {
    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(
      45,
      window.innerWidth / window.innerHeight,
      0.1,
      1000
    );

    renderer = new THREE.WebGLRenderer();
    renderer.setClearColor(0x000000, 1.0);
    renderer.physicallyCorrectLights = true;
    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    renderer.shadowMap.enabled = true;
    renderer.toneMapping = THREE.ReinhardToneMapping;
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);

    var planeGeometry = new THREE.PlaneGeometry(20, 20);
    var planeMaterial = new THREE.MeshLambertMaterial({ color: 0xcccccc });
    var plane = new THREE.Mesh(planeGeometry, planeMaterial);
    plane.receiveShadow = true;
    plane.rotation.x = -0.5 * Math.PI;
    plane.position.x = 0;
    plane.position.y = -2;
    plane.position.z = 0;

    scene.add(plane);

    var cubeGeometry = new THREE.BoxGeometry(6, 6, 2);
    var cubeMaterial = new THREE.MeshLambertMaterial({
      color: 0x086113,
      transparent: true,
      opacity: 1,
    });
    var cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
    cube.name = "cube";
    cube.castShadow = true;

    scene.add(cube);

    camera.position.x = 15;
    camera.position.y = 16;
    camera.position.z = 13;
    camera.lookAt(scene.position);

    var ambient = new THREE.AmbientLight(0xffffff, 0.3);
    scene.add(ambient);
    var light = new THREE.DirectionalLight(0xffffff, 1, 100, 2);
    light.position.set(10, 20, 20);
    light.castShadow = true;
    scene.add(light);

    control = new (function () {
      this.rotationSpeed = 0.5;
      this.opacity = 1;
      this.color = 0x086113;
      this.message = "cube";
      this.speed = 0;
    })();
    addControlGui(control);

    clock = new THREE.Clock();
    document.body.appendChild(renderer.domElement);

    render();
  }

  function addControlGui(controlObject) {
    var gui = new dat.GUI();
    gui.add(controlObject, "rotationSpeed", -1, 1);
    gui.add(controlObject, "opacity", 0.1, 1);
    gui.add(controlObject, "message", ["cube", "sphere", "plane"]);
    gui.add(controlObject, "speed", { stopped: 0, slow: 0.1, fast: 5 });
    gui.addColor(controlObject, "color");
  }

  function render() {
    var delta = clock.getDelta();
    var rotSpeed = delta * control.rotationSpeed * control.speed;
    camera.position.x =
      camera.position.x * Math.cos(rotSpeed) +
      camera.position.z * Math.sin(rotSpeed);
    camera.position.z =
      camera.position.z * Math.cos(rotSpeed) -
      camera.position.x * Math.sin(rotSpeed);
    camera.lookAt(scene.position);

    scene.getObjectByName("cube").material.opacity = control.opacity;
    scene.getObjectByName("cube").material.color = new THREE.Color(
      control.color
    );

    requestAnimationFrame(render);
    renderer.render(scene, camera);
  }

  function handleResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
  }

  window.addEventListener("DOMContentLoaded", function (event) {
    init();
  });
  window.addEventListener("resize", handleResize, false);
}
